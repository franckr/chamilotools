#!/usr/bin/env python2

import sys
import os
TEST_DIR = os.path.dirname(__file__)
SCRIPT_DIR = os.path.join(os.path.dirname(__file__), "..")
SCRIPT = os.path.join(SCRIPT_DIR, "chamilotools")
sys.path.append(SCRIPT_DIR)

from chamilolib.create_quiz_lib import (
    openQuiz,
    quiz2yaml
)
from chamilolib.yaml_quiz import (
    yaml2quiz
)

from chamilolib.cli import (
    CLIOptions
)
from chamilolib.CampusChamiloInstance import CampusChamiloInstance
from chamilolib.GrenobleINPChamiloInstance import GrenobleINPChamiloInstance
from chamilolib.ChamiloInstance import ChamiloInstance
from testCreateDownloadQuiz import TEST_FILES


import unittest
import subprocess


try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')


class TestQuiz2Yaml2Quiz(unittest.TestCase):
    def runTest(self):
        for basename in TEST_FILES:
            file = os.path.join(os.path.dirname(__file__), basename)
            print "Testing %s ..." % file
            d, a = openQuiz(file)
            y = quiz2yaml(d, a)
            D, A = yaml2quiz(y)
            self.assertEquals(D, d)
            self.assertEquals(A, a)


class TestYaml2Yaml(unittest.TestCase):
    def testCleanup(self):
        cmd = [SCRIPT, 'create-quiz', '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml', '--cleanup']
        out = subprocess.check_output(cmd, stderr=DEVNULL)
        self.assertEquals(out, """questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")

    def testDump(self):
        cmd = [SCRIPT, 'create-quiz', '-i',
               os.path.join(TEST_DIR, 'qcm_python.yml'),
               '--dump-yaml']
        out = subprocess.check_output(cmd, stderr=DEVNULL)
        self.assertEquals(out, """attempts: 0
description: ''
feedbackFinal: ''
questions:
- answers:
  - feedback: Non, pas toujours.
    ko: contient toujours un nombre entier
  - feedback: En effet (mais une variable peut contenir d'autres valeurs)
    ok: peut contenir un nombre entier
  - ko: est visible depuis l'ensemble du programme
  category: ''
  description: ''
  score: 1
  title: En Python, une variable ...
  type: E
title: QCM sur le cours 1

""")


class TestCLI(unittest.TestCase):
    def testDefault(self):
        o = CLIOptions(['create-quiz'])
        self.assertEquals(o.username, '')
        self.assertEquals(o.password, '')
        self.assertEquals(o.courseName, '')
        self.assertEquals(o.filenames, [])
        self.assertEquals(o.wiki, False)
        self.assertEquals(o.feedback, True)
        self.assertEquals(o.nb, 0)
        self.assertEquals(o.mode, "2")
        self.assertEquals(o.passPercent, "0")
        self.assertEquals(o.timeLimit, "0")
        self.assertEquals(o.onePage, False)
        self.assertEquals(o.randomCategory, False)
        self.assertEquals(o.addMode, 'add')
        self.assertEquals(o.dumpYaml, False)
        self.assertEquals(o.debugMode, False)
        self.assertEquals(o.cleanUp, False)
        self.assertEquals(o.randomAnswers, False)
        self.assertEquals(o.view, False)
        self.assertEquals(o.hidden, False)
        self.assertEquals(o.urlBase, None)
        self.assertEquals(o.debug, False)
        self.assertEquals(o.verbose, 0)
        self.assertEquals(o.instance, None)
        self.assertEquals(o.instanceName, '')

    def testAll(self):
        o = CLIOptions("""
            create-quiz
            -d -w -u username -p 99
            -i i-in-file -i i-in-file2 file1 file2
            -c coursename -t 42 -l 12 -f -a -o -r -m 1
            --dump-yaml --cleanup
            --random-answers --view --hidden --url URL
            --debug --verbose --verbose
            """.split())
        self.assertEquals(o.username, 'username')
        self.assertEquals(o.password, '')
        self.assertEquals(o.courseName, 'coursename')
        self.assertEquals(o.filenames, ['i-in-file', 'i-in-file2', 'file1', 'file2'])
        self.assertEquals(o.wiki, True)
        self.assertEquals(o.feedback, False)
        self.assertEquals(o.nb, 42)
        self.assertEquals(o.mode, "0")
        self.assertEquals(o.passPercent, "99")
        self.assertEquals(o.timeLimit, "12")
        self.assertEquals(o.onePage, True)
        self.assertEquals(o.randomCategory, True)
        self.assertEquals(o.addMode, 'remove-same')
        self.assertEquals(o.dumpYaml, True)
        self.assertEquals(o.debugMode, True)
        self.assertEquals(o.cleanUp, True)
        self.assertEquals(o.randomAnswers, True)
        self.assertEquals(o.view, True)
        self.assertEquals(o.hidden, True)
        self.assertEquals(o.urlBase, 'URL')
        self.assertEquals(o.debug, True)
        self.assertEquals(o.verbose, 2)
        self.assertIsInstance(o.instance, ChamiloInstance)

    def testCampus(self):
        o = CLIOptions("create-quiz --campus".split())
        self.assertIsInstance(o.instance, CampusChamiloInstance)
        self.assertEquals(o.instanceName, 'Campus')

    def testInstance(self):
        o = CLIOptions("create-quiz --instance GrenobleINP".split())
        self.assertIsInstance(o.instance, GrenobleINPChamiloInstance)
        self.assertEquals(o.instanceName, 'GrenobleINP')

    def testMode(self):
        def test_one_mode(opt, val):
            o = CLIOptions(['create-quiz', opt])
            self.assertEquals(o.mode, val)
        test_one_mode('-a', "0")
        test_one_mode('-s', "1")
        test_one_mode('-e', "2")

    def testDownload(self):
        o = CLIOptions("""
            download-quiz
            --exercise-id 4242
            """.split())
        self.assertEquals(o.exerciseId, 4242)

    def testGlobal(self):
        o = CLIOptions("""-v -u some-user -c some-course --debug -d
            --url some-url --campus
            --password-from-stdin
            --instance Campus
            create-quiz
            -v -v
            """.split())
        self.assertEquals(o.username, 'some-user')
        self.assertEquals(o.courseName, 'some-course')
        self.assertEquals(o.debugMode, True)
        self.assertEquals(o.debug, True)
        self.assertEquals(o.urlBase, 'some-url')
        self.assertIsInstance(o.instance, CampusChamiloInstance)
        self.assertEquals(o.useCampus, True)
        self.assertEquals(o.passwordFromStdin, True)
        self.assertEquals(o.verbose, 3)

    def testGlobalOverride(self):
        o = CLIOptions("""-u some-user -c some-course --debug -d
            --url some-url
            --password-from-stdin
            --instance Campus
            create-quiz
            -u real-user -c real-course --debug -d --url real-url
            --instance GrenobleINP
            -v -v
            """.split())
        self.assertEquals(o.username, 'real-user')
        self.assertEquals(o.courseName, 'real-course')
        self.assertEquals(o.debugMode, True)
        self.assertEquals(o.debug, True)
        self.assertEquals(o.urlBase, 'real-url')
        self.assertEquals(o.instanceName, 'GrenobleINP')
        self.assertIsInstance(o.instance, GrenobleINPChamiloInstance)
        self.assertEquals(o.passwordFromStdin, True)
        self.assertEquals(o.verbose, 2)


class TestCheck(unittest.TestCase):
    def test_check_ok(self):
        cmd = [
            SCRIPT, 'check-quiz',
            os.path.join(TEST_DIR, 'qcm_python.yml'),
            os.path.join(TEST_DIR, 'quiz_for_test.xls')
        ]
        out = subprocess.check_output(cmd, stderr=DEVNULL).split('\n')
        self.assertRegexpMatches(out[0], 'Checking .*/qcm_python.yml ...')
        self.assertRegexpMatches(out[1], 'Checking .*/quiz_for_test.xls ...')

    def test_check_ko(self):
        ok = False
        try:
            cmd = [
                SCRIPT, 'check-quiz',
                os.path.join(TEST_DIR, 'err/yaml_syntax_error.yml')
            ]
            subprocess.check_output(cmd, stderr=DEVNULL)
        except subprocess.CalledProcessError:
            ok = True
        self.assertTrue(ok)


if __name__ == "__main__":
    unittest.main()
