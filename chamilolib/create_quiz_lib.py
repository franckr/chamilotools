#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import print_function

import re
import sys
import webbrowser
import copy
import traceback
from chamilolib.cli import (
    CLIOptions,
    CLIError
)
from chamilolib.utilities import (
    LoadError,
    encode,
    error,
    warn
)
from chamilolib.xl_quiz import (
    openQuizXls
)
from chamilolib.yaml_quiz import (
    openQuizYaml,
    quiz2yaml,
    pretty_yaml,
    dump_yaml
)
from chamilolib.list_quiz import list_quiz
from chamilolib.chamiloBrowser import LinkNotFoundError
import chamilolib.syntax


# import logging
# logger = logging.getLogger("mechanize")
# logger.addHandler(logging.StreamHandler(sys.stdout))
# logger.setLevel(logging.DEBUG)


def openQuiz(path):
    if re.match('.*\.xlsx?', path):
        d, a = openQuizXls(path)
    elif re.match('.*\.yml', path):
        d, a = openQuizYaml(path)
    else:
        print("Please, provide a XLS(X) or a Yaml file describing the quizz.")
        raise CLIError()
    return checkAndFillQuiz(d, a)


def convert_desc(s, syntax, q_type, field, o):
    return chamilolib.syntax.convert_desc(
        o.instance, s, syntax, q_type, field, o.courseName)


def create_new_choice(br, o, ask, feedback, descriptionQuiz):
    syntax = descriptionQuiz.get("syntax")
    syntax = ask.get("syntax", syntax)
    syntax_answer = ask.get("syntaxAnswer", syntax)
    syntax_desc = ask.get("syntaxDesc", syntax)

    # Make a new choice. See doc/question-types.md for the meaning of
    # question names.

    # T : Text to be complete (blank part)
    # A : Elements to be associated
    # O : Open ask (no point by default, need the correction of teacher)
    # C : Only one solution to be selected
    # U : Only One one solution with the possibility to select "I don't know"
    # M : Multiple choices are possibles, each true answer has positive points, each wrong answer has negative points
    # E : Multiple choices are possibles, the points are affected only if the exact combination is selected
    # G : Multiple choices are possibles, the points affected is function depending of the part of exact combination
    # MVF : Multiple choices are possibles, similar M but with "I don't know" choice
    # MVFE : Multiple choices are possibles, similar E  but with "I don't
    # know" choice
    type = ask.get("type", "C")

    # Create new question
    if type == "U":
        answerType = 10
    elif type == "O":
        answerType = 5
    elif type == "M":
        answerType = 2
    elif type == "E":
        answerType = 9
    elif type == "G":
        answerType = 14
    elif type == "MVF":
        answerType = 11
    elif type == "MVFE":
        answerType = 12
    elif type == "A":
        answerType = 4
    elif type == "T":
        answerType = 3
    else:
        answerType = 1
        type = "C"
    br.follow_link(url_regex="answerType=" + str(answerType))
    # Set to the right number of questions
    nb_answers = len(ask["answers"])
    form = br.select_form('form')

    # Workaround for a bug (in BeautifulSoup?) which makes fields end
    # up outside the <form></form> part of the HTML tree.
    # There's just one form, let's just reattach everything to the form:
    br.reattach_tag_to_form("input")
    br.reattach_tag_to_form("text")
    br.reattach_tag_to_form("textarea")
    br.reattach_tag_to_form("select")

    # Read the page to find value for categories
    soup = br.current_page
    dicCat = {x.string: x["value"] for x in soup.find(
        'select', {"name": "questionCategory"}).findAll('option')}
    form.input({"questionName": encode(ask["title"])})
    form.textarea({"questionDescription": encode(convert_desc(
        ask["description"], syntax_desc, type, "description", o))})
    if ask["category"]:
        form.input({"questionCategory": dicCat[ask["category"]]})
    if type == "O":
        pass
    elif type == "T":
        answer_text = convert_desc(ask["answers"],
                                   syntax_answer, type,
                                   "answer", o)
        br["answer"] = answer_text
        # Possible separators with Chamilo
        separators = {'[': "0",
                      '{': "1",
                      '(': "2",
                      '*': "3",
                      '#': "4",
                      '%': "5",
                      '$': "6"}
        sep_regex = {'[': r"\[(.*?)\]",
                     '{': r"\{(.*?)\}",
                     '(': r"\((.*?)\)",
                     '*': r"\*(.*?)\*",
                     '#': r"#(.*?)#",
                     '%': r"%(.*?)%",
                     '$': r"\$(.*?)\$"}
        sep = ask['separator']
        br["select_separator"] = [separators[sep]]
        i = 0
        for word in re.findall(sep_regex[sep], answer_text):
            br.new_control(
                "text", "sizeofinput[" + str(i) + "]",
                value=o.instance.text_field_width(len(word)))
            br.new_control(
                "text", "weighting[" + str(i) + "]",
                value=str(ask["score"]))
            i += 1
    elif type == "A":
        # Delete existing controls to re-create them later
        for i in br.current_page.find_all("input"):
            if i.attrs['name'].startswith("option["):
                i.extract()
        for s in br.current_page.find_all("select"):
            if s.attrs['name'].startswith("matches["):
                s.extract()
        a = ask["answers"]
        left = [x["text"] for x in a if x["text"]]
        right = {x["correct"] for x in a}
        left = {x: i + 1 for x, i in zip(left, range(len(left)))}
        # form.find_control("nb_matches").readonly = False
        br.new_control("text", "nb_matches", value=str(len(left)))
        # form.find_control("nb_options").readonly = False
        br.new_control("text", "nb_options", value=str(len(right)))
        i = 1
        new_right = dict()
        for option in right:
            br.new_control(
                "text", "option[" + str(i) + "]", value=encode(option))
            new_right[option] = i
            i += 1
        right = new_right
    else:
        # form.find_control("nb_answers").readonly = False
        br.new_control("text", "nb_answers", value=str(nb_answers))
        br.current_page.find('input', {'name': 'nb_answers'})["value"] = str(nb_answers)
    if type != "T" and type != "O":
        i = 1
        for q in ask["answers"]:
            text = q["text"]
            correct = q["correct"]
            if type != "A":
                br.new_control(
                    "hidden", "counter[" + str(i) + "]", value=str(i))
                br.new_control(
                    type="text",
                    name="comment[" + str(i) + "]",
                    value=ask["feedback"] if feedback else "")
            if "feedback" in q:
                br["comment[" + str(i) + "]"] = encode(q["feedback"])
            try:
                br["answer[" + str(i) + "]"] = convert_desc(
                    text, syntax_answer,
                    type,
                    "answer", o)
            except:
                value = convert_desc(text, syntax_answer, type, "answer", o)
                br.new_control(
                    "text", "answer[" + str(i) + "]", value=value)
            if type != "E" and type != "G" and type != "MVF" and type != "MVFE":
                try:
                    # TODO: allow per-answer score.
                    br["weighting[" + str(i) + "]"] = str(ask["score"]) if correct else "-" + ask[
                        "score"] if type == "M" else "0"
                except:
                    br.new_control("text", "weighting[" + str(i) + "]",
                                   value=ask["score"] if correct else "-" + str(ask["score"]) if type == "M" else "0")
            if type == "A":
                br.new_control(
                    "text", "matches[" + str(i) + "]", value=encode(right[correct]))
            elif type == "MVF":
                br.new_control(
                    "checkbox", "correct[" + str(i) + "]", value="1" if correct else "2", checked="checked")
            elif correct:
                if type == "M" or type == "E" or type == "G" or type == "MVFE":
                    br.new_control(
                        "checkbox", "correct[" + str(i) + "]", value="1", checked="checked")
                else:
                    br.new_control("checkbox", "correct", value=str(i), checked="checked")
            i += 1
    if type == "E":
        # Control is outside <form></form>
        br["weighting[1]"] = str(ask["score"])
    if type == "G" or type == "MVFE":
        br.new_control("text", "weighting[1]", value=str(ask["score"]))
    if type == "G":
        if "noNegative" in ask and ask["noNegative"]:
            br["pts"] = True
        br.new_control("text", "weighting[1]", value="10")
    if type == "U":
        # To add only if is an unique choice with I don't know
        br.new_control("hidden", "counter[666]", value="666")
        br.new_control("text", "answer[666]", value="Ne sais pas")
        br.new_control("text", "comment[666]",
                       {"value": encode(ask["feedback"]) if feedback else ""})
        br.new_control("text", "weighting[666]", value="0")
        # End of the part to add only for unique choice with I don't know
    if type == "MVF":
        # Use score by default, but allow the user to set
        # correct/incorrect/don't know specific scores.
        scoreCorrect = ask["score"]
        scoreIncorrect = - ask["score"]
        scoreDontKnow = 0
        if "scoreCorrect" in ask:
            scoreCorrect = ask["scoreCorrect"]
        if "scoreIncorrect" in ask:
            scoreIncorrect = ask["scoreIncorrect"]
        if "scoreDontKnow" in ask:
            scoreDontKnow = ask["scoreDontKnow"]
        br.new_control("text", "option[1]", value=str(scoreCorrect))
        br.new_control("text", "option[2]", value=str(scoreIncorrect))
        br.new_control("text", "option[3]", value=str(scoreDontKnow))
#    if type=="G":
#        print(forms().next())
    br.submit_selected(btnName="submitQuestion", data={'answerType': answerType})
    # Return to asks list. Be careful, there are several admin.php
    # links on the page.
    br.follow_link(url_regex=r"(^|/)admin\.php\?exerciseId=")


def setDefault(d, k, v):
    if k not in d:
        # Use a copy to avoid aliases, which would confuse the Yaml
        # dumper.
        d[k] = copy.copy(v)


QUIZ_DEFAULTS = {
    'attempts': 0,
    'feedbackFinal': '',
    'description': '',
}

QUESTION_DEFAULTS = {
    'score': 1,
    'category': '',
    'description': '',
}

QUESTION_TYPE_DEFAULTS = {
    'T': {'separator': '%'},
}


def checkAndFillQuiz(d, a):
    for q in a:
        i = 0
        if 'title' not in q:
            i += 1
            error("Please provide a title for question %d\n%s" % (i, pretty_yaml(q)))
        if 'score' not in q:
            warn("No score set for question \"%s\"." % q['title'])
        q_default = QUESTION_TYPE_DEFAULTS.get(q['type'], dict())
        default = dict(QUESTION_DEFAULTS, **q_default)
        for k in default:
            setDefault(q, k, default[k])
    if 'title' not in d:
        error("Please provide a title for the quiz")
    for k in QUIZ_DEFAULTS:
        setDefault(d, k, QUIZ_DEFAULTS[k])
    return d, a


def cleanDefaultValues(d, a):
    for q in a:
        q_default = QUESTION_TYPE_DEFAULTS.get(q['type'], dict())
        default = dict(QUESTION_DEFAULTS, **q_default)
        for k in default:
            if q[k] == default[k]:
                del q[k]
    for k in QUIZ_DEFAULTS:
        if d[k] == QUIZ_DEFAULTS[k]:
            del d[k]
    return d, a


def connectOrResetChamilo(br, o):
    if br is None:
        return connectChamilo(o)
    else:
        resetChamilo(br, o.courseName)
        return br


def resetChamilo(br, courseName):
    return br.open(br.instance.get_url_course(courseName))


def resetToExercices(br, courseName):
    resetChamilo(br, courseName)
    br.follow_link(url_regex=r"/exercice\.php\?.*cidReq=" + courseName)


def deleteOrphans(br, courseName):
    url = (
        br.instance.get_url_base() +
        "/main/coursecopy/recycle_course.php?cidReq=" +
        courseName + "&id_session=0&gidReq=0"
    )
    br.open(url)
    br.select_form('form')
    br["recycle_option"] = "select_items"
    br.submit_selected()
    br.select_form('form')
    br["resource[quiz][-1]"] = "on"
    br.submit_selected()


def connectChamilo(o):
    br = o.instance.connect(o)
    br.instance = o.instance
    br.open(br.instance.get_entry_url_course(o.courseName))
    resetChamilo(br, o.courseName)
    return br


def buildFeedback(feedbackGeneral, asks, syntax, o=None):
    feedback = "<h2 id='w_retour-global'> " + feedbackGeneral + "</h2><ul>"
    i = 1
    for ask in asks:
        if ask["type"] != "O":
            feedback += (
                "<li> " + "<div class='ok'>Question " + str(i) + " : " +
                convert_desc(ask.get("feedback", ""),
                             syntax, ask["type"],
                             "feedback", o) +
                "</div>" + "<div class='ko'>Question " +
                str(i) + " : " +
                convert_desc(ask.get("feedbackFalse", ""),
                             syntax, ask["type"],
                             "feedback", o) +
                "</div></li>"
            )
        else:
            feedback += (
                "<li> " + "<div>Question " +
                str(i) + " : " +
                convert_desc(ask.get("feedback", ""),
                             syntax, ask["type"],
                             "feedback", o) +
                "</div></li>"
            )
        i += 1
    feedback += r"""</ul><script>$( function(){
  wrong_answer=$("span[style*='line-through']")
  wrong_answer.parent().children().hide()
  wrong_answer.show()
  okAnswer=$("h3:contains('Exact')").parents(".question_row,.question_row_answer").find(".page-header h3,.page-header h4")
  okAnswer.each(function() {
    indice=$(this).text()
    indice=indice.substr(0,indice.indexOf('.'))
    $('#w_retour-global').parent().find("li:nth-child("+indice+") .ko").hide()
  })
  koAnswer=$("h3:contains('Faux')").parents(".question_row,.question_row_answer").find(".page-header h3,.page-header h4")
  koAnswer.each(function() {
    indice=$(this).text()
    indice=indice.substr(0,indice.indexOf('.'))
    $('#w_retour-global').parent().find("li:nth-child("+indice+") .ok").hide()
  })
})</script>
"""
    return feedback


def createCategory(br, asks):
    br.follow_link(url_regex=r"/exercice\.php")
    br.follow_link(url_regex=r"(^|/)tests_category\.php")
    soup = br.current_page
    categories = {c["category"] for c in asks if c["category"]}
    for c in categories:
        if not soup.findAll('div', {"class": "sectiontitle"}, text=c):
            br.follow_link(url_regex=r"\?action=addcategory")
            br.select_form('form')
            br["category_name"] = c
            br.submit_current()


def createQuiz(descriptionQuiz, asks, o, br=None):
    syntax = descriptionQuiz.get("syntax")
    br = connectOrResetChamilo(br, o)
    createCategory(br, asks)
    resetToExercices(br, o.courseName)
    # Delete if exist
    if o.addMode in (CLIOptions.REMOVE_SAME, CLIOptions.REMOVE_ORPHAN):
        idsQuiz = list_quiz(
            o, search_title=descriptionQuiz["title"],
            br=br,
            reset=False)
        for id in idsQuiz:
            br.follow_link(url_regex="choice=delete.*exerciseId=" + id)
            # Following the link brings us back to a page with the
            # list => no need to "br.back()".
    elif o.addMode == CLIOptions.CHECK:
        idsQuiz = list_quiz(
            o, search_title=descriptionQuiz["title"],
            br=br,
            reset=False)
        if len(idsQuiz) > 0:
            raise LoadError("Exercise exists")
    br.follow_link(url_regex=r"(^|/)exercise_admin\.php")
    if descriptionQuiz["feedbackFinal"] == "Retour global:":
        feedback = buildFeedback(
            descriptionQuiz["feedbackFinal"], asks, syntax, o)
    else:
        feedback = descriptionQuiz["feedbackFinal"]
    br.select_form('form')
    br["exerciseTitle"] = encode(descriptionQuiz["title"])
    br["exerciseDescription"] = convert_desc(
        descriptionQuiz["description"],
        syntax,
        "toplevel", "description",
        o)
    br["exerciseFeedbackType"] = ["0" if o.feedback else "2"]
    br["results_disabled"] = [o.mode]
    br["display_category_name"] = ["0"]
    br["randomAnswers"] = ["1" if o.randomAnswers else "0"]
    br["randomByCat"] = ["1" if o.randomCategory else "0"]
    br["randomQuestions"] = ["1" if o.randomCategory else "0"]
    if o.timeLimit != "0":
        br.find_control(name="enabletimercontrol").items[0].selected = True
        br["enabletimercontroltotalminutes"] = o.timeLimit
    br["pass_percentage"] = o.passPercent
    br["exerciseType"] = ["1" if o.onePage else "2"]
    br["exerciseAttempts"] = [str(descriptionQuiz["attempts"])]
    br["text_when_finished"] = feedback
    br.submit_selected()

    # Get the "preview" full URL
    l = br.find_link(url_regex=r"(^|/)overview\.php")
    urlQuiz = br.absolute_url(l.get('href'))

    # Which exercice are we in?
    match = re.match('.*exerciseId=([0-9]*).*', br.geturl())
    if match:
        idQuiz = match.group(1)
    else:
        raise AssertionError("URL " + br.geturl() +
                             " does not contain exerciseId=...")

    if o.hidden:
        br.follow_link(
            url_regex=r"/exercice\.php\?.*cidReq=" + o.courseName)
        br.follow_link(
            url_regex=r"(^|/)exercice\.php\?.*&choice=disable&.*&exerciseId=" + idQuiz)
        br.follow_link(
            url_regex=r"(^|exercice/)admin\.php\?.*&exerciseId=" + idQuiz)

    for ask in asks:
        try:
            create_new_choice(br, o, ask, False, descriptionQuiz)
        except:
            print("Error question in Chamilo:")
            print(pretty_yaml(ask))
            traceback.print_exc(file=sys.stdout)
            raise LoadError()
    if o.addMode == CLIOptions.REMOVE_ORPHAN:
        deleteOrphans(br, o.courseName)
        resetToExercices(br, o.courseName)
    return urlQuiz, idQuiz, br


def createQuizFromXls(filename, options):
    (d, a) = openQuizXls(filename)
    return createQuiz(d, a, options)


def deleteQuiz(o, br=None, reset=True):
    if reset:
        br = connectOrResetChamilo(br, o)
        br.follow_link(url_regex=r"/exercice\.php\?.*cidReq=" + o.courseName)
    try:
        br.follow_link(url_regex='choice=delete.*&exerciseId=' +
                       str(o.exerciseId) + '$')
    except LinkNotFoundError:
        print("No such exercice: " + str(o.exerciseId))
        return br, ""
    return br


def cmd_delete_quiz(o):
    o.read_password()
    br = connectChamilo(o)
    if o.all:
        d = {}
        ids = list_quiz(o, br=br, dict_quiz=d)
        if not ids:
            print("No quiz to delete")
        for i in ids:
            o.exerciseId = i
            print("Deleting quiz " + i + ":", d[i], "...", end='')
            deleteQuiz(o, br=br, reset=False)
            print(' done')
    else:
        deleteQuiz(o)


def processQuiz(o, filename):
    d, a = openQuiz(filename)
    if o.debugMode:
        print(a)
        sys.exit()
    d["attempts"] = o.nb
    if o.wiki:
        d["syntax"] = "mediawiki"
    if o.dumpYaml:
        if o.cleanUp:
            d, a = cleanDefaultValues(d, a)
        y = quiz2yaml(d, a)
        print(dump_yaml(y))
    else:
        o.read_password()
        # NEEDSWORK: we could reuse the same connection to Chamilo to
        # save a few HTTP round-trips.
        url, idQuiz, br = createQuiz(d, a, o)
        if o.view:
            webbrowser.open(url)
        else:
            print(url)


def checkQuiz(filename):
    d, a = openQuiz(filename)


def process_all_quiz(o):
    for filename in o.filenames:
        processQuiz(o, filename)


def cmd_create_quiz(o):
    if not o.filenames:
        print("Please provide at least one quizz file as argument")
        raise CLIError()
    if o.dumpYaml:
        return process_all_quiz(o)
    if not o.username:
        raise CLIError("Please provide a username (-u ...)")
    if not o.courseName:
        raise CLIError("Please provide a course name (-c ...)")
    process_all_quiz(o)


def cmd_check_quiz(o):
    if not o.filenames:
        print("Please provide at least one quizz file as argument")
        raise CLIError()
    for filename in o.filenames:
        print("Checking", filename, "...")
        checkQuiz(filename)
