#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import os
import errno
TEST_DIR = os.path.abspath(os.path.dirname(__file__))
SCRIPT_DIR = os.path.join(os.path.dirname(__file__), "..")
SCRIPT = os.path.abspath(os.path.join(SCRIPT_DIR, "chamilotools"))
sys.path.append(SCRIPT_DIR)

import unittest
from subprocess import Popen, PIPE, call
import getpass
from string import strip
from chamilolib.cli import CLIOptions

read_password = getpass.getpass
PASSWORD = None

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    DEVNULL = open(os.devnull, 'wb')


def run_command(args, input_text):
    p = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate(input_text)
    print(stderr, end='')
    return stdout


class TestUpload(unittest.TestCase):
    # Test using run_command to check the behavior and the CLI at the
    # same time.
    def upload_zip(self, filename):
        cmd = [SCRIPT, 'upload-zip',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               filename]
        # print(' '.join(cmd))
        return run_command(cmd, PASSWORD)

    def upload_file(self, filename):
        cmd = [SCRIPT, 'upload-file',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               filename]
        # print(' '.join(cmd))
        return run_command(cmd, PASSWORD)

    def delete_doc(self, filename, fuzzy=False):
        cmd = [SCRIPT, 'delete-doc',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               filename
               ]
        if fuzzy:
            cmd.append('--fuzzy-file-name')
        # print(' '.join(cmd))
        return run_command(cmd, PASSWORD)

    def get_doc(self, filename):
        cmd = [SCRIPT, 'get-doc',
               '--password-from-stdin',
               '-c', o.courseName, '-u', o.username,
               '--instance', o.instanceName,
               filename
               ]
        # print(' '.join(cmd))
        return run_command(cmd, PASSWORD)

    def testUploadZip(self):
        global PASSWORD
        try:
            os.remove("dirtoupload.zip")
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise
        call(['zip', '-r', 'dirtoupload.zip', 'dirtoupload'])

        if PASSWORD is None:
            PASSWORD = read_password()

        out = self.upload_zip('dirtoupload.zip')
        self.assertTrue(
            # Chamilo 1.9.10.2
            strip(out) == 'File upload succeeded! index' or
            # campus.chamilo.org as of August 26, 2016
            strip(out) == 'File upload succeeded!')

        out = self.get_doc('dirtoupload/index.html')
        self.assertEquals(out, "Some content\n")

        out = self.delete_doc('dirtoupload').split('\n')
        self.assertRegexpMatches(out[0], 'Deleting file.*')
        self.assertRegexpMatches(out[1], '^\s*Document deleted: dirtoupload$')

        out = self.delete_doc('dirtoupload')
        self.assertEquals(out, 'ERROR: Could not find file dirtoupload\n')

    def testUploadFile(self):
        global PASSWORD
        os.chdir(TEST_DIR)

        if PASSWORD is None:
            PASSWORD = read_password()

        out = self.upload_file('dirtoupload/index.html')
        self.assertTrue(
            # Chamilo 1.9.10.2
            strip(out).startswith('File upload succeeded! index') or
            # campus.chamilo.org as of August 26, 2016
            out == '\n')

        out = self.get_doc('index.html')
        self.assertEquals(out, "Some content\n")

        out = self.upload_file('dirtoupload2/index.html')
        self.assertTrue(
            # Chamilo 1.9.10.2
            strip(out).startswith('File upload succeeded! index') or
            # campus.chamilo.org as of Sept 5th, 2016
            out == '\n')

        out = self.get_doc('index.html')
        self.assertEquals(out, "New content\n")

        out = self.delete_doc('index')
        self.assertEquals(out, 'ERROR: Could not find file index\n')

        out = self.delete_doc('index', fuzzy=True).split('\n')
        self.assertRegexpMatches(out[0], 'Deleting file.*')
        self.assertRegexpMatches(out[1], '^\s*Document deleted: index$')

        out = self.upload_file('dirtoupload/index.html')

        out = self.delete_doc('index.html').split('\n')
        self.assertRegexpMatches(out[0], 'Deleting file.*')
        self.assertRegexpMatches(out[1], '^\s*Document deleted: index$')

if __name__ == "__main__":
    global o
    o = CLIOptions(stop_before_parse=True)
    known, unknown_args = o.parser.parse_known_args(
        ['upload-zip', 'dummy.zip'] + sys.argv[1:],
        namespace=o)
    if known.passwordFromStdin:
        def readline_and_strip():
            line = sys.stdin.readline()
            if line[-1] == '\n':
                return line[:-1]
        read_password = readline_and_strip
    unittest.main(argv=[sys.argv[0]] + unknown_args)
