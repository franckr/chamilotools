#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import difflib
import re
import os

TEST_DIR = os.path.dirname(__file__)
sys.path.append(os.path.join(TEST_DIR, '..'))

from chamilolib.download_quiz import downloadQuiz
from chamilolib.create_quiz_lib import (
    openQuiz,
    createQuiz,
    deleteQuiz,
    CLIOptions,
)

from chamilolib.GrenobleINPChamiloInstance import GrenobleINPChamiloInstance

from chamilolib.utilities import (
    AuthenticationError
)

TEST_FILES = [
    'tricky_questions.yml',
    'qcm_python.yml',
    'quiz_jquery.xls',
    'quiz_jquery.yml',
    'quiz_for_test.xls',
    'qcm_code.yml',
    'all_types.yml'
]


def main(args):
    global br
    o = CLIOptions(['create-quiz'] + args)
    if o.instance is None:
        o.instance = GrenobleINPChamiloInstance()
    if not (o.username and o.courseName):
        print "Please, use -u and -c to specify test course"
        sys.exit()
    o.read_password()
    br = None
    for f in TEST_FILES:
        print "Testing %s ..." % f,
        sys.stdout.flush()
        d, a = openQuiz(os.path.join(TEST_DIR, f))
        refpath = os.path.join(TEST_DIR, 'ref', o.instanceName, '%s.xml' % f)
        print "importing ...",
        sys.stdout.flush()
        o.addMode = "1"
        o.onePage = True
        out, o.exerciseId, br = createQuiz(d, a, o, br=br)
        print "downloading ...",
        sys.stdout.flush()
        br, xml = downloadQuiz(o, br=br)
        # Remove unique identifier
        # (would trigger spurious diffs)
        xml = re.sub('ident="EXO_[0-9]*"', 'ident="EXO_N"', xml)
        xml = re.sub('([Ii]dentifier="QST_)[0-9]*"', '\\1N"', xml)
        ok = False
        try:
            expected = open(refpath).read()
            if xml == expected:
                ok = True
            else:
                print "ERROR"
                for line in difflib.unified_diff(expected.split('\n'),
                                                 xml.split('\n'),
                                                 fromfile='expected',
                                                 tofile='actual'):
                    print line
                reffd = open(refpath + '.actual', 'w')
                reffd.write(xml)
                reffd.close()
                print "New content stored in %s for debug" % (refpath + '.actual')
                print
        except IOError:
            reffd = open(refpath, 'w')
            reffd.write(xml)
            reffd.close
            print "NEW REFERENCE"
        print "deleting ...",
        sys.stdout.flush()
        if ok:
            br = deleteQuiz(o, br=br)
            print "ok"


if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except AuthenticationError:
        sys.exit(1)
