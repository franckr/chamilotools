# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import print_function

import re
import mechanicalsoup
import tempfile
from subprocess import call
import sys
from six.moves import urllib


class LinkNotFoundError(BaseException):
    pass


class ChamiloBrowser(mechanicalsoup.Browser):
    def __init__(self):
        super(ChamiloBrowser, self).__init__()
        # self.set_handle_robots(False)
        self.debug = False
        self.verbose = 0
        self.current_page = None
        self.current_url = None
        self.current_item = None

    def open(self, url, *args, **kwargs):
        if self.verbose == 1:
            sys.stdout.write('.')
            sys.stdout.flush()
        elif self.verbose >= 2:
            print(url)

        resp = self.get(url, *args, **kwargs)
        if hasattr(resp, 'soup'):
            self.current_page = resp.soup
        self.current_url = resp.url
        self.current_item = None
        return resp

    def geturl(self):
        return self.current_url

    def forms(self):
        return self.current_page.find_all('form')

    def select_form(self, *args, **kwargs):
        found_forms = self.current_page.select(*args, **kwargs)
        if len(found_forms) < 1:
            if self.debug:
                print('select_form failed for', *args)
                self.launch_browser()
            raise LinkNotFoundError()

        f = found_forms[0]
        if 'action' in f:
            f['action'] = self.absolute_url(f['action'])
        else:
            f['action'] = self.current_url
        self.current_item = mechanicalsoup.Form(f)
        return self.current_item

    def reattach_tag_to_form(self, *args, **kwargs):
        """Attach some tags to the currently selected form.

        For some reason (bug in BeautifulSoup?), some form fields end up
        outside the form in the document. This function allows
        reattaching them to the currently selected form.
        """
        all_items = set(self.current_item.form.descendants)
        for t in self.current_page.find_all(*args, **kwargs):
            if t not in all_items:
                self.current_item.form.append(t)

    def __setitem__(self, name, value):
        return self.set(name, value)

    def set(self, name, value, force=False):
        if self.current_item is None:
            print("Please select a form first")
            raise NotImplementedError()
        input = self.current_item.form.find("input", {"name": name})
        if input:
            if input.attrs['type'] in ("radio", "checkbox"):
                for option in self.current_item.form.find_all("input", {"name": name}):
                    if "checked" in option.attrs:
                        del option.attrs["checked"]
                selected = self.current_item.form.find("input", {"name": name, "value": value})
                if selected:
                    selected.attrs["checked"] = "checked"
                else:
                    input.attrs["checked"] = "checked"
                    input["value"] = value
            else:
                input["value"] = value
            return
        textarea = self.current_item.form.find("textarea", {"name": name})
        if textarea:
            textarea.string = value
            return
        select = self.current_item.form.find("select", {"name": name})
        if select:
            for option in select.find_all("option"):
                if "selected" in option.attrs:
                    del option.attrs["selected"]
            select.find("option", {"value": value}).attrs["selected"] = "selected"
            return
        if force:
            self.new_control('input', name, value=value)
            return
        if self.debug:
            print("No input named", name, "found.")
            # self.launch_browser()
        raise LinkNotFoundError()

    def new_control(self, type, name, value, **kwargs):
        old = self.current_page.find('input', {'name': name})
        if old:
            old.decompose()
        old = self.current_page.find('textarea', {'name': name})
        if old:
            old.decompose()
        control = self.current_page.new_tag('input')
        control['type'] = type
        control['name'] = name
        control['value'] = value
        for k, v in kwargs.items():
            control[k] = v
        self.current_item.form.append(control)
        return control

    def submit_selected(self, btnName=None, *args, **kwargs):
        if btnName is not None:
            if 'data' not in kwargs:
                kwargs['data'] = dict()
            kwargs['data'][btnName] = ''

        # Work around an encoding bug in resquests if method is unicode.
        self.current_item.form.attrs['method'] = str(
            self.current_item.form.attrs['method'])

        resp = self.submit(self.current_item, url=self.current_url, *args, **kwargs)
        self.current_url = resp.url
        self.current_page = resp.soup
        self.current_item = None
        return resp

    def launch_browser(self):
        # Launch a browser on the guilty page.
        print("Launching browser on local copy of", self.current_url)
        with tempfile.NamedTemporaryFile(delete=False) as file:
            file.write(self.current_page.encode())
        call(['sensible-browser', file.name])

    def list_links(self):
        print("Links in the current page:")
        for l in self.links():
            print("    ", l)

    def links(self, url_regex=None):
        if url_regex is not None:
            res = []
            for a in self.current_page.find_all('a', href=True):
                if re.search(url_regex, a['href']):
                    res.append(a)
            return res
        return self.current_page.find_all('a', href=True)

    def find_link(self, url_regex=None):
        for a in self.links():
            if url_regex is not None:
                if re.search(url_regex, a['href']):
                    return a
            else:
                raise NotImplementedError()
        raise LinkNotFoundError()

    def absolute_url(self, url):
        """Make url absolute. url can be either relative or absolute."""
        return urllib.parse.urljoin(self.current_url, url)

    def follow_link(self, url_regex=None):
        try:
            link = self.find_link(url_regex)
            return self.open(self.absolute_url(link['href']))
        except LinkNotFoundError:
            if self.debug:
                print('follow_link failed for', url_regex)
                self.list_links()
                # from chamilolib.utilities import display_html
                # display_html(self.current_page.encode(), '')
                self.launch_browser()
            raise

    def set_debug(self, debug):
        self.debug = debug

    def set_verbose(self, verbose):
        self.verbose = verbose
