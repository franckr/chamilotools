# Useful documentations

* [Mechanical Soup](https://github.com/hickford/MechanicalSoup) :
  [example](https://github.com/hickford/MechanicalSoup/blob/master/example.py), [tests](https://github.com/hickford/MechanicalSoup/tree/master/tests)

# Obsolete documentation (we don't use mechanize anymore)

* [Mechanize cheat sheet](http://www.pythonforbeginners.com/cheatsheet/python-mechanize-cheat-sheet)

* [Mechanize API documentation](http://joesourcecode.com/Documentation/mechanize0.2.5/)
  and in particular the
  [Browser class](http://joesourcecode.com/Documentation/mechanize0.2.5/mechanize._mechanize.Browser-class.html).
