# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import print_function

import re
import bs4

from chamilolib.utilities import toUnicode


def cmd_list_quiz(o):
    o.read_password()
    d = {}
    ids = list_quiz(o, dict_quiz=d, search_title=o.quizName)
    for i in ids:
        print(i, d[i])


def get_quiz_title(l):
    # The link text may be truncated (end replaced
    # with "..."). The title="..." attribute seems
    # reliable when present.
    for k, v in l.attrs.items():
        if k == 'title':
            return toUnicode(v)

    # No "title=" attribute. The title was not truncated,
    # we can look at the link text.
    i = 1  # l.contents[0] is the <img> tag.
    if l.contents[0] == '\n':
        # At least on campus.chamilo.org, there's a newline beforme
        # the <img> tag.
        i = 2
    if l.contents[i] == ' ':
        i += 1
    text = l.contents[i]
    if isinstance(text, bs4.element.Tag):
        # Example with hidden quiz: text is surrounded with
        # <font>...</font>
        text = text.string
    # The actual text contains <span ... display:none>X Results? not
    # reviewed</span>, but mechanize removes the <span> and </span>
    # tags in the text attribute.
    return toUnicode(
        re.findall(' (.*?) *([0-9]* Results? not reviewed)?$', text)[0][0]
    )


def list_quiz(o, br=None, search_title=None, dict_quiz=None, reset=True):
    if reset:
        import chamilolib.create_quiz_lib as cqc
        br = cqc.connectOrResetChamilo(br, o)
        cqc.resetToExercices(br, o.courseName)
    idsQuiz = []
    for l in br.links(url_regex=r"(^|/)overview\.php"):
        if search_title is not None or dict_quiz is not None:
            actual_title = get_quiz_title(l)
        if search_title is not None and search_title != actual_title:
            continue
        id = re.findall("exerciseId=([0-9]+)", l['href'])[0]
        idsQuiz.append(id)
        if dict_quiz is not None:
            dict_quiz[id] = get_quiz_title(l)
    return idsQuiz
