# Creating Yaml and XL quizz with `chamilotools create-quiz` #

`chamilotools create-quiz` is a command that allows you to write Quizz
(the "Exercices" or "Test" tool in Chamilo) offline, and send the whole Quizz to
Chamilo with a single command. Quizz can be written in several
formats:

* A spreadsheet format (MS Excel or LibreOffice Calc), probably most
  suited for non-computer scientists. It is compatible with Chamilo's
  native XL import format, but `chamilotools create-quiz` can deal with more
  types of questions.

* A textual format using the [Yaml](http://www.yaml.org/) markup
  language. This is most suited if you like working with a text
  editor, and if you want to use version control (Git, ...) to share
  the quizz files with other user and expect concurrent edits and
  merge to work.

## Example ##

In practice, a Quizz in spreadsheet form looks like this:

![Spreadsheet Quizz](quizz-XL.png "A Quizz Using the SpreadSheet format")

The exact quizz used in the picture can be downloaded here:
[quizz_python.xls](quizz_python.xls). One can then send the quiz to
Chamilo like this (to be executed in a terminal):

    $ chamilotools create-quiz -u login -c course --view -o
    Password:

The `-u` option allows specifying the username (the one you use to log
in on Chamilo when using your browser), the `-c` specifies the course
name (it appears in the URL of every course). Both options are
mandatory.

In addition, we used `--view` to launch a browser on the imported
quizz when the import is complete, and `-o` which means "All questions
on a single page".

The
[resulting quizz](http://chamilotools.gitlab.io/chamilotools/quizz_python.html)
looks like this:

![Quizz in Chamilo](quizz.png "Resulting Quizz in Chamilo")

Alternatively, the same quizz can be written in Yaml:

```Yaml
title: Python & Computer science basics
syntax: mediawiki
questions:
- type: E
  score: 5
  title: Fractions
  description: Calculez la valeur <math>\frac{1/2}{1/3}</math> en Python.
  answers:
    - ko: 1 / 2 / 1 / 3
      feedback: Non, cette expression est équivalent à ((1 / 2) / 1) / 3.
    - ok: (1 / 2) / (1 / 3)
    - ko: (1 // 2) // (1 // 3)
      feedback: Non, // est la division entière en Python.
    - ko: 1 // 2 // 1 // 3
- type: T
  score: 8
  title: Completez le programme suivant
  syntax: pre
  answers: |
    a = "Bonjour"
    n = len(a)
    b = ""
    while n > 0%:%
        b = b + a[n-1]
        n = n - 1
  feedbackTrue: Good!
  feedbackFalse: You need to use the proper operator (/ is float division, // is integer division) and parenthesis
- type: T
  score: 5
  title: "Convert the following numbers to decimal:"
  description: underlined numbers use the binary notation
  answers: |
    <u>10101</u> = %21%
    
    <u>1111</u> = %15%
```

(You can download the file from here: [quizz_python.yml](quizz_python.yml))

## Command-line Syntax ##

Run `chamilotools create-quiz --help` to see the possible options of the tool.

## Yaml Syntax ##

Documentation and example for the Yaml syntax : [yaml-syntax.md](yaml-syntax.md).

## More examples ##

* A real-life example of quizz about Python (french only):
  [python-fr](python-fr) (generated quizz are available on the page
  [of the Python course in Grenoble INP's CPP](http://chamilo2.grenet.fr/inp/main/exercice/exercice.php?cidReq=CPP1A1CMINFO)).

* An XL quiz covering all types of questions: [quiz_for_test.xls](../test/quiz_for_test.xls)

* A Yaml quiz covering all types of questions:
  [all_types.yml](../test/all_types.yml) (generated page:
  [all_types.html](http://chamilotools.gitlab.io/chamilotools/all_types.html))
